ENTITIES = []


def init():
    from qbubbles.bubbles import Bubble
    from qbubbles.entities import Player

    ENTITIES.append(Bubble())
    ENTITIES.append(Player())
    return ENTITIES
