import typing as _t
import tkinter
import typing

import qbubbles
import qbubbles.entities
import qbubbles.entities
import qbubbles.events
import qbubbles.exceptions
# from qbubbles.registry import Registry
import qbubbles.registries
import qbubbles.util


class BubbleType(qbubbles.entities.EntityType):
    def __init__(self):
        self.priority = 0

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 5
        self.maxSpeed: int = 10
        self.hardness: int = 1
        self.damage: int = 1

        # Multipliers
        self.scoreMultiplier: int = 0
        self.attackMultiplier: int = 0
        self.defenceMultiplier: int = 1

        self.__key: typing.Optional[qbubbles.util.NamespacedKey] = None

    @property
    def key(self) -> qbubbles.util.NamespacedKey:
        return self.__key

    @key.setter
    def key(self, name) -> typing.NoReturn:
        # for symbol in name:
        #     if symbol not in string.ascii_letters + string.digits + "_" + ":":
        #         raise UnlocalizedNameError(f"Invalid character '{symbol}' for unlocalized name '{name}'")
        # if name[0] not in string.ascii_letters:
        #     raise UnlocalizedNameError(f"Invalid start character '{name[0]}' for unlocalized name '{name}'")
        # if name[-1] not in string.ascii_letters + string.digits:
        #     raise UnlocalizedNameError(f"Invalid start character '{name[-1]}' for unlocalized name '{name}'")
        # if self.__key is not None:
        #     raise IllegalStateError(f"Unlocalized name already set!")

        self.__key = name

    def set_uname(self, name):
        self.key = qbubbles.util.NamespacedKey.from_key(name)

    def get_key(self):
        return self.key

    # def get_uname_registry(self) -> List[str]:
    #     return Registry.get_id_bubble(self)

    def __repr__(self) -> str:
        return f"<BubbleType({self.get_key()})>"

    def on_collision(self, bubbleobject: 'Bubble', other: 'LivingEntity'):
        pass


class Bubble(qbubbles.entities.LivingEntity):
    def __init__(self, baseobject: BubbleType = None, maxhealth=5, basehealth=None, radius=5, speed=5, health=5,
                 scoremp=None, attackmp=None, defencemp=None):
        """
        Bubble object, used to create bubble on the game canvas / window.

        Attributes:
          appliedEffects:
            Description: Dictionary of applied-effects, key is the instance, value is the class. \n
            Type: ``Dict[AppliedEffect, Type[AppliedEffect]]``
          baseSpeed:
            Description: Base speed of the bubble, must not be changed after creation. \n
            Type: ``int``
          baseObject:
            Description: Base object of the bubble, must not be changed after creation. \n
            Type: ``Bubble`` (Or a subclass of it)
          baseRadius:
            Description: Base radius of the bubble, must not be changed after creation. \n
            Type: ``int``
          baseHealth:
            Description: The starting health of the bubble, must not be changed after creation. \n
            Type: ``float`` or ``int``.

        :param baseobject:
        :param maxhealth:
        :param basehealth:
        """
        # Static attributes
        import qbubbles

        super(Bubble, self).__init__()

        self.noCollisionWith = [Bubble]

        # Set uname.
        self.set_uname("bubble")

        # Key and pause.
        self.__key: qbubbles.util.NamespacedKey
        self._pause = False

        # Enity name and data.
        self._entityName = "bubble"
        self._entityData = {"objects": [], "speed_multiplier": 0.5, "id": self.get_key().__str__()}

        # Base attributes
        self.baseSpeed: typing.Optional[int] = None
        self.baseObject: BubbleType = baseobject
        self.baseRadius: typing.Optional[int] = None
        self.baseHealth = maxhealth if basehealth is None else basehealth

        # Switches
        self.allowCollision = True
        self.isInvulnerable = False

        # Dynamic attributes
        self.appliedEffects = {}
        self.maxHealth = maxhealth
        self.radius: typing.Optional[int] = None
        self.speed: typing.Optional[int] = None

        # Modifier attributes
        self.scoreMultiplier: float = self.baseObject.scoreMultiplier if self.baseObject is not None else None

        # Bubble displaying attributes
        self.imageList = {}
        self.id: typing.Optional[int] = None
        if baseobject is not None:
            # Add modifiers.
            self.scoreMultiplier = scoremp if scoremp is not None else self.baseObject.scoreMultiplier
            self.attackMultiplier = attackmp if attackmp is not None else self.baseObject.attackMultiplier
            self.defenceMultiplier = defencemp if defencemp is not None else self.baseObject.defenceMultiplier

            # self._objectData = {"Position": (None, None),
            #                     "Effects": [],
            #                     "Abilities": [],
            #                     "Attributes": {
            #                         "radius": None,
            #                         "health": None,
            #                         "speed": None
            #                     },
            #                     "Bases": {
            #                         "speed": None,
            #                         "radius": None,
            #                         "health": None
            #                     },
            #                     "Modifiers": {
            #                         "regenMultiplier": 0,
            #                         "scoreMultiplier": 0,
            #                         "attackMultiplier": 0,
            #                         "defenceMultiplier": 0
            #                     },
            #                     "Switches": {
            #                         "allowCollision": True,
            #                         "isInvulnerable": False
            #                     },
            #                     "ID": baseobject.get_uname()}

        # Base attributes.
        self.baseSpeed = speed
        self.baseRadius = int(radius / 2)
        self.baseRadiusF = radius / 2

        # Dynamic attributes.
        self.health = health
        self.speed = speed
        self.radius = int(radius / 2)
        self.radiusF = radius / 2

        # # Todo: Remove when object-data is fully using the get_objectdata method.
        # self._objectData["radius"] = radius
        # self._objectData["speed"] = speed
        # self._objectData["health"] = health
        # self._objectData["Position"] = (x, y)

    def reload(self, odata: dict):
        # raise Exception("Test exception, reload is called!")

        if self.id is not None:
            raise OverflowError(f"BubbleObject is already created")
        
        for effectdata in odata["Effects"]:
            effect: qbubbles.effects.BaseEffect = qbubbles.registries.Effects.get(effectdata["id"])

            # Start the effect using saved effect data. Using the object-data (odata) given from the arguments.
            if effectdata["duration"] > 0.0:
                self.start_effect(
                    effect, qbubbles.registries.Scenes.get("game"), effectdata["duration"], effectdata["strength"],
                    **dict(effectdata["extradata"]))

        # print(odata)

        attributes = odata["Attributes"]
        self.speed = attributes["speed"]
        self.radius = attributes["radius"]
        self.radiusF = attributes["radiusF"]
        self.health = attributes["health"]
        self.maxHealth = attributes["maxHealth"]

        bases = odata["Bases"]
        self.baseSpeed = bases["speed"]
        self.baseHealth = bases["health"]
        self.baseRadius = bases["radius"]
        self.baseRadiusF = bases["radiusF"]

        modifiers = odata["Modifiers"]
        self.scoreMultiplier = modifiers["scoreMultiplier"]
        self.regenMultiplier = modifiers["regenMultiplier"]
        self.attackMultiplier = modifiers["attackMultiplier"]
        self.defenceMultiplier = modifiers["defenceMultiplier"]

        switches = odata["Switches"]
        self.allowCollision = switches["allowCollision"]
        # self.isInvulnerable = switches["isInvulnerable"]

        # Create bubble image.
        self.id = qbubbles.registries.Scenes.get("game").canvas.create_image(
            *odata["Position"], image=qbubbles.registries.Textures.get(
                "qbubbles:bubble", self.baseObject.get_key().get_key(), radius=int(self.radiusF * 2)))

        self.teleport(*odata["Position"])

        # Activate Events.
        qbubbles.events.UpdateEvent.bind(self.on_update)
        qbubbles.events.CleanUpEvent.bind(self.on_cleanup)
        qbubbles.events.CollisionEvent.bind(self.on_collision)
        qbubbles.events.PauseEvent.bind(self.on_pause)

    def add_effect(self, appliedeffect: qbubbles.effects.AppliedEffect):
        """
        Adds an effect, must be an AppliedEffect()-instance. For starting a new effect, use start_effect() instead.

        :param appliedeffect:
        :return:
        """

        self.appliedEffects[appliedeffect] = appliedeffect.baseObject
        # self.appliedEffectTypes.append(type(appliedeffect))

    def remove_effect(self, appliedeffect: qbubbles.effects.AppliedEffect):
        """
        Removes an applied-effect from the player's effect list.

        :param appliedeffect: The applied-effect to remove.
        :return:
        """

        # index = self.appliedEffects.index(appliedeffect)
        del self.appliedEffects[appliedeffect]

    def start_effect(self, effect_class: qbubbles.effects.BaseEffect, scene, duration: float, strength: _t.Union[float, int],
                     **extradata) -> qbubbles.effects.AppliedEffect:
        """
        Starts an effect, it converts the BaseEffect() subclass into an AppliedEffect() instance. And starts the effect.

        :param effect_class: The base-class of the effect.
        :param scene: The game-scene.
        :param duration: The duration of the effect.
        :param strength: The strength of the effect
        :param extradata: The extra data to add to the effect.
        :raises AssertionError: If the effect-class is a type.
        :returns: The applied-effect
        """

        assert not isinstance(effect_class, type)

        appliedeffect = qbubbles.effects.AppliedEffect(effect_class, scene, duration, strength, self, **extradata)
        self.appliedEffects[appliedeffect] = appliedeffect.baseObject

        return appliedeffect

    # noinspection PyDictCreation
    def get_objectdata(self):
        odata = {}

        if self.baseObject is not None:
            # Position, Effects and Abilities
            odata["Position"] = self.get_coords()
            odata["Effects"] = [appliedeffect.get_data() for appliedeffect in self.appliedEffects]
            odata["Abilities"] = [ability.get_data() for ability in self.abilities]

            # Bases
            bases = {}
            bases["speed"] = self.baseSpeed
            bases["radius"] = self.baseRadius
            bases["radiusF"] = self.baseRadiusF
            bases["health"] = self.baseHealth
            odata["Bases"] = bases

            # Attributes
            attributes = {}
            attributes["maxHealth"] = self.maxHealth
            attributes["health"] = self.health
            attributes["radius"] = self.radius
            attributes["radiusF"] = self.radiusF
            attributes["speed"] = self.speed
            odata["Attributes"] = attributes

            switches = {}
            switches["allowCollision"] = self.allowCollision
            odata["Switches"] = switches

            # Modifiers
            modifiers = {}
            modifiers["regenMultiplier"] = self.regenMultiplier
            modifiers["scoreMultiplier"] = self.scoreMultiplier
            modifiers["attackMultiplier"] = self.attackMultiplier
            modifiers["defenceMultiplier"] = self.defenceMultiplier
            odata["Modifiers"] = modifiers

            # ID
            odata["ID"] = self.baseObject.get_key().__str__()

        return odata

    # noinspection PyDictCreation
    def get_entitydata(self):
        sdata = {}
        sdata["objects"] = []
        sdata["speedMultiplier"] = 0.5
        sdata["id"] = self.get_key().__str__()

        return sdata

    def on_collision(self, evt: qbubbles.events.CollisionEvent):
        # pass
        if self.allowCollision:
            if evt.eventObject == self and evt.collidedObj != self:
                self.baseObject.on_collision(self, evt.collidedObj)

    def on_pause(self, evt: qbubbles.events.PauseEvent):
        self._pause = evt.pause

    def create(self, x, y):
        if self.baseObject is None:
            raise qbubbles.exceptions.UnlocalizedNameError(f"BubbleObject is initialized as use for Entity information, "
                                       f"use the baseobject argument with "
                                       f"a Bubble-instance instead of NoneType to fix this problem")
        if self.id is not None:
            raise OverflowError(f"BubbleObject is already created")
        canvas: tkinter.Canvas = qbubbles.registries.Scenes.get("qbubbles:game").canvas

        # Logging.debug("BubbleObject", f"Creation RadiusF: {self.radiusF}")
        # Logging.debug("BubbleObject", f"Creation Radius: {self.radius}")

        # print(self.baseObject.get_key())

        # Create bubble image.
        self.id = canvas.create_image(
            x, y, image=qbubbles.registries.Textures.get("qbubbles:bubble", self.baseObject.get_key(), radius=self.radiusF * 2))

        # Activate Events.
        qbubbles.events.UpdateEvent.bind(self.on_update)
        qbubbles.events.CleanUpEvent.bind(self.on_cleanup)
        qbubbles.events.CollisionEvent.bind(self.on_collision)
        qbubbles.events.PauseEvent.bind(self.on_pause)

        # print(f"Created Bubble\n Bubble Object Representation: {repr(self)}")

    def on_update(self, evt: qbubbles.events.UpdateEvent):
        # game_map = Scenes.get("game").gameType
        if not self._pause:
            spd_mpy = evt.scene.gameType.player.score / 10000
            spd_mpy /= 2
            if spd_mpy < 0.5:
                spd_mpy = 0.5
            self.move(-self.baseSpeed * evt.dt * spd_mpy, 0)

    def save(self):
        return dict(self._entityData)

    def on_cleanup(self, evt: qbubbles.events.CleanUpEvent):
        if self.dead:
            qbubbles.events.UpdateEvent.unbind(self.on_update)
            qbubbles.events.CleanUpEvent.unbind(self.on_cleanup)
            qbubbles.events.CollisionEvent.unbind(self.on_collision)
            qbubbles.events.PauseEvent.unbind(self.on_pause)
            self.delete()


class NormalBubble(BubbleType):
    def __init__(self):
        super(NormalBubble, self).__init__()

        self.priority = 1500000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96
        self.scoreMultiplier: float = 1
        self.attackMultiplier: float = 0

        self.set_uname("normal_bubble")


class DoubleBubble(BubbleType):
    def __init__(self):
        super(DoubleBubble, self).__init__()

        self.priority = 150000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96
        self.scoreMultiplier: float = 2
        self.attackMultiplier: float = 0

        self.set_uname("double_value")


class TripleBubble(BubbleType):
    def __init__(self):
        super(TripleBubble, self).__init__()

        self.priority = 100000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96
        self.scoreMultiplier: float = 3
        self.attackMultiplier: float = 0

        self.set_uname("triple_value")


class DoubleStateBubble(BubbleType):
    def __init__(self):
        super(DoubleStateBubble, self).__init__()

        self.priority = 15000000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96

        self.scoreMultiplier: float = 2
        self.attackMultiplier: float = 0

        self.set_uname("double_state")

    def on_collision(self, bubbleobject: Bubble, other_object: qbubbles.entities.LivingEntity):
        if other_object.get_key() == "qbubbles:player":
            other_object: qbubbles.entities.Player
            scene = qbubbles.registries.Scenes.get("qbubbles:game")
            other_object.start_effect(qbubbles.effects.ScoreMultiplierEffect(), scene,
                                      scene.gameType.randoms["qbubbles:effect.duration"][0].randint(12, 17), 2)


class TripleStateBubble(BubbleType):
    def __init__(self):
        super(TripleStateBubble, self).__init__()

        self.priority = 10000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96

        self.scoreMultiplier: float = 3
        self.attackMultiplier: float = 0

        self.set_uname("triple_state")

    def on_collision(self, bubbleobject: Bubble, other_object: qbubbles.entities.LivingEntity):
        if other_object.get_key() == "player":
            other_object: qbubbles.entities.Player
            scene = qbubbles.registries.Scenes.get("qbubbles:game")
            other_object.start_effect(qbubbles.effects.ScoreMultiplierEffect(), scene,
                                      scene.gameType.randoms["qbubbles:effect.duration"][0].randint(7, 10), 3)


class DamageBubble(BubbleType):
    def __init__(self):
        super(DamageBubble, self).__init__()

        self.priority = 1000000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96

        self.scoreMultiplier: float = 0.5
        self.attackMultiplier: float = 1

        self.set_uname("damage_bubble")

        # raise RuntimeError("This is shit")


class HealBubble(BubbleType):
    def __init__(self):
        super(HealBubble, self).__init__()

        self.priority = 100000

        self.minRadius: int = 21
        self.maxRadius: int = 80
        self.minSpeed: int = 40
        self.maxSpeed: int = 96

        self.scoreMultiplier: float = 0.5
        self.attackMultiplier: float = -1

        self.set_uname("healer_bubble")

        # raise RuntimeError("This is shit")


class SpeedboostBubble(BubbleType):
    def __init__(self):
        super(SpeedboostBubble, self).__init__()

        self.priority = 15000

        self.minRadius: int = 5
        self.maxRadius: int = 50
        self.minSpeed: int = 116
        self.maxSpeed: int = 228

        self.scoreMultiplier: float = 3
        self.attackMultiplier: float = 0

        self.set_uname("speedboost")

    # noinspection PyTypeChecker
    def on_collision(self, bubbleobject: Bubble, other_object: qbubbles.entities.LivingEntity):
        if other_object.get_key() == "qbubbles:player":
            other_object: qbubbles.entities.Player
            scene = qbubbles.registries.Scenes.get("qbubbles:game")
            other_object.start_effect(qbubbles.effects.ScoreMultiplierEffect(), scene,
                                      scene.gameType.randoms["qbubbles:effect.duration"][0].randint(6, 9), 3)


class TeleportBubble(BubbleType):
    def __init__(self):
        super(TeleportBubble, self).__init__()

        self.priority = 10

        self.minRadius: int = 5
        self.maxRadius: int = 30
        self.minSpeed: int = 136
        self.maxSpeed: int = 204

        self.scoreMultiplier: float = 1
        self.attackMultiplier: float = 0

        self.set_uname("teleport_bubble")

    def on_collision(self, bubbleobject: Bubble, other_object: qbubbles.entities.Player):
        if other_object.get_key() == "qbubbles:player":
            other_object.get_ability("teleport")["value"] = \
                bubbleobject.baseSpeed / bubbleobject.baseObject.maxSpeed * 2


if __name__ == '__main__':
    bub_type = BubbleType()
    bub_type.key = "bubble"
    print(bub_type)
