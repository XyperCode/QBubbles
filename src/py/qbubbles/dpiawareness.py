import re
import unittest.case


def get_hwnd_dpi(window_handle):
    # To detect high DPI displays and avoid need to set Windows compatibility flags
    import os
    if os.name == "nt":
        from ctypes import windll, pointer, wintypes
        windll.shcore.SetProcessDpiAwareness(1)
        DPI100pc = 96  # DPI 96 is 100% scaling
        DPI_type = 0  # MDT_EFFECTIVE_DPI = 0, MDT_ANGULAR_DPI = 1, MDT_RAW_DPI = 2
        winH = wintypes.HWND(window_handle)
        monitorhandle = windll.user32.MonitorFromWindow(winH, wintypes.DWORD(2))  # MONITOR_DEFAULTTONEAREST = 2
        X = wintypes.UINT()
        Y = wintypes.UINT()
        try:
            windll.shcore.GetDpiForMonitor(monitorhandle, DPI_type, pointer(X), pointer(Y))
            return X.value, Y.value, (X.value + Y.value) / (2 * DPI100pc)
        except Exception:
            return 96, 96, 1  # Assume standard Windows DPI & scaling
    else:
        return None, None, 1  # What to do for other OSs?


def tk_geometry_scale(s, cvtfunc):
    patt = r"(?P<W>\d+)x(?P<H>\d+)\+(?P<X>\d+)\+(?P<Y>\d+)"  # format "WxH+X+Y"
    R = re.compile(patt).search(s)
    G = str(cvtfunc(R.group("W"))) + "x"
    G += str(cvtfunc(R.group("H"))) + "+"
    G += str(cvtfunc(R.group("X"))) + "+"
    G += str(cvtfunc(R.group("Y")))
    return G


def make_tk_dpi_aware(TKGUI):
    TKGUI.DPI_X, TKGUI.DPI_Y, TKGUI.DPI_scaling = get_hwnd_dpi(TKGUI.winfo_id())
    TKGUI.tk_scale = lambda v: int(float(v) * TKGUI.DPI_scaling)
    TKGUI.tk_geometry_scale = lambda s: tk_geometry_scale(s, TKGUI.tk_scale)


class __Test(unittest.case.TestCase):
    @staticmethod
    def test1():
        """
        First Test.

        @return: None
        """

        import tkinter

        gui = tkinter.Tk()
        make_tk_dpi_aware(gui)  # Sets the windows flag + gets adds .DPI_scaling property
        gui.geometry(gui.tk_geometry_scale("600x200+200+100"))
        gray = "#cccccc"
        demo_frame = tkinter.Frame(gui, width=gui.tk_scale(580), height=gui.tk_scale(180), background=gray)
        demo_frame.place(x=gui.tk_scale(10), y=gui.tk_scale(10))
        demo_frame.pack_propagate(False)
        label_text = "Scale = " + str(gui.DPI_scaling)
        demo_label = tkinter.Label(demo_frame, text=label_text, width=10, height=1, font=("Arial", gui.tk_scale(10)))
        demo_label.pack(pady=gui.tk_scale(70))

    @staticmethod
    def test2():
        """
        Second Test.

        @return:
        """

        from tkinter import Tk, Button
        root = Tk()
        make_tk_dpi_aware(root)
        root.geometry(root.tk_geometry_scale("200x24+100+100"))
        button = Button(root, text="HoiHallo", font=("Helvetica", int(10*root.DPI_scaling)))
        button.pack()
