from tkinter import Frame, Canvas

from typing import Optional, Callable, Any

from qbubbles.exceptions import SceneNotFoundError, IllegalStateError
from qbubbles.registries import Scenes
# from qbubbles.registry import Registry
from qbubbles.util import NamespacedKey


class SceneManager(object):
    INSTANCE: 'SceneManager'

    def __init__(self):
        if hasattr(SceneManager, "INSTANCE"):
            raise IllegalStateError("SceneManager() instance already created.")

        self._scenes = {}
        self.currentScene: Optional[Scene] = None
        self.currentSceneName: Optional[str] = None

        SceneManager.INSTANCE = self

    def change_scene(self, name, *args, **kwargs):
        if isinstance(name, str):
            if NamespacedKey.from_string(name) not in Scenes.keys():
                raise SceneNotFoundError(f"scene '{name}' is not existent")
        elif isinstance(name, NamespacedKey):
            if name not in Scenes.keys():
                raise SceneNotFoundError(f"scene '{name}' is not existent")

        # Hide old scene first
        if self.currentScene is not None:
            self.currentScene.hide_scene()

        # Get new scene and show it
        new_scene = Scenes.get(name)
        self.currentSceneName = name
        self.currentScene: Scene = new_scene
        self.currentScene.show_scene(*args, **kwargs)


class Scene(object):
    scenemanager = SceneManager()
    Scenes.set_manager(scenemanager)

    def __init__(self, root):
        self.frame = Frame(root)
        self.__key: Optional[NamespacedKey] = None

    def hide_scene(self):
        self.frame.pack_forget()

    def show_scene(self, *args, **kwargs):
        self.frame.pack(fill="both", expand=True)

    def bind_event(self, event: str, func: Callable[[Any], Any], add=None) -> str:
        """
        Binds an event to a function or method.

        @param event: The type of the event.
        @param func: The function to bind the event to.
        @param add:
        @return: The function ID, used for unbind the event.
        """

        return self.frame.bind(event, func, add)

    def unbind_event(self, event: str, funcid: str = None):
        """
        Unbinds am event from a function using the function ID.

        @param event: The type of the event to unbind.
        @param funcid: The function ID returned from the event binding.
        @return:
        """

        self.frame.unbind(event, funcid)

    def start_scene(self):
        pass

    def update(self):
        pass

    def tick_update(self):
        pass

    def __repr__(self):
        return f"<Scene({self.__class__.__name__})>"

    def set_uname(self, uname):
        self.__key = NamespacedKey.from_key(uname)

    def get_key(self):
        return self.__key


class CanvasScene(Scene):
    def __init__(self, root):
        super(CanvasScene, self).__init__(root)
        self.canvas = Canvas(self.frame, highlightthickness=0)
        self.canvas.pack(fill="both", expand=True)

    def __repr__(self):
        return super(CanvasScene, self).__repr__()
