import os

import qbubbles.testing

if __name__ == '__main__':
    import sys
    import platform

    sys.argv.append(rf"--debug")

    if platform.system() == "Windows":
        sys.argv.append(rf"gameDir=C:\Users\{os.getlogin()}\AppData\Roaming\.qbubbles.test")
    elif platform.system() == "linux":
        sys.argv.append(rf"gameDir=/home/{os.getlogin()}/.qbubbles.test")
    else:
        raise qbubbles.exceptions.PlatformNotSupported()

    qbubbles.testing.Main()
