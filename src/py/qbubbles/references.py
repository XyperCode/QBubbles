from pathlib import Path

import qbubbles
import qbubbles.registries


class References(object):
    INSTANCE: 'References' = None

    def __init__(self):
        if References.INSTANCE is not None:
            raise qbubbles.exceptions.IllegalStateError("References() class already initialized")

        self.addonsFolder: Path = Path(qbubbles.registries.LAUNCHER_CONFIG["gameDir"]) / "addons"
        self.gameDir: Path = Path(qbubbles.registries.LAUNCHER_CONFIG["gameDir"])
        self.savesDir: Path = Path(qbubbles.registries.LAUNCHER_CONFIG["gameDir"]) / "saves"

        References.INSTANCE = self
