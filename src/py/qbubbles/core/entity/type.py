class Type(object):
    def __init__(self, type_: int):
        self.__type = type_

    def __int__(self):
        return self.__type

    def __eq__(self, other: 'Type'):
        if isinstance(other, Type):
            return self.__type == other.get_int()
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_int(self):
        return self.__type

    def __hash__(self):
        return self.__type,


NEUTRAL = Type(0)
MIXED = Type(1)
AGRESSIVE = Type(2)
NOCLIPPED = Type(3)
