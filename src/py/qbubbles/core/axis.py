class Axis(object):
    def __init__(self, axis=0):
        self.__axis: int = axis

    # noinspection PyProtectedMember
    def __eq__(self, other: 'Axis'):
        if isinstance(other, Axis):
            return self.__axis == other._Axis__axis
        return False

    def __ne__(self, other: 'Axis'):
        return not (self.__eq__(other))

    def __neg__(self):
        if self == HORIZONTAL:
            return VERTICAL
        elif self == VERTICAL:
            return HORIZONTAL
        else:
            raise ValueError("Unknown axis number: " + self.__axis.__str__())

    def __hash__(self):
        return self.__axis,


HORIZONTAL = Axis(0)
VERTICAL = Axis(1)
