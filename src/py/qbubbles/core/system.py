from array import array
from collections.abc import Mapping as _Mapping
from typing import Iterator as _Iterator, Set as _Set, Generic, KT, VT, TypeVar, Dict as _Dict, ValuesView, AbstractSet, \
    Tuple, Optional, overload, Union

_T_co = TypeVar("_T_co")
_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_VT_co = TypeVar("_VT_co")
_T = TypeVar("_T")

from numpy import short as _short, long as _long, int as _int, float as _float

# noinspection PyUnusedName
String = str
# noinspection PyUnusedName
Integer = _int
# noinspection PyUnusedName
Float = _float
# noinspection PyUnusedName
Long = _long
# noinspection PyUnusedName
Short = _short
# noinspection PyUnusedName
Dictionary = dict
# noinspection PyUnusedName
List = list
# noinspection PyUnusedName
Array = array
# noinspection PyUnusedName
Set = set
# noinspection PyUnusedName
Class = type


class MappingMeta(Generic[KT, VT]):
    def __getitem__(self, key: KT) -> VT:
        pass

    def __getitem__(self, k: KT) -> VT:
        pass

    def __setitem__(self, k: KT, v: VT):
        pass

    def __len__(self) -> int:
        pass
        # return self.__map.__len__()

    def __iter__(self) -> None:
        pass

    def get_entryset(self) -> '_Set[EntryMeta[KT, VT]]':
        e: set = set()

        for k, v in self.__map.items():
            e.add(Entry(k, v))

        return e

    def get_keyset(self) -> _Set[KT]:
        s: set = set()

        for k in self.__map.keys():
            s.add(k)

        return s

    def get_valueset(self) -> _Set[KT]:
        s: set = set()

        for v in self.__map.values():
            s.add(v)

        return s


class EntryMeta(Generic[KT, VT]):
    def __init__(self, key, value):
        pass

    def get_key(self) -> VT:
        pass

    def get_value(self) -> VT:
        pass


class Entry(object):
    def __init__(self, key, value):
        # super().__init__(key, value)
        self.__k: _KT = key
        self.__v: _VT_co = value

    def get_key(self):
        return self.__k

    def get_value(self):
        return self.__v

    def __repr__(self):
        return f"({repr(self.__k)};{repr(self.__v)})"

    def __str__(self):
        return f"{repr(self.__k)};{repr(self.__v)}"


class Map(_Mapping):
    def __init__(self, dict: _Dict[KT, VT]):
        self.__map = dict

    def __getitem__(self, k: _KT) -> _VT_co:
        return self.__map[k]
        # return

    def __setitem__(self, k: _KT, v: _VT_co):
        self.__map[k] = v
        # return

    def __len__(self) -> int:
        return self.__map.__len__()

    def __iter__(self):
        return None

    def get_entryset(self):
        e: set = set()

        for k, v in self.__map.items():
            e.add(Entry(k, v))

        return e

    def get_keyset(self) -> _Set[KT]:
        s: set = set()

        for k in self.__map.keys():
            s.add(k)

        return s

    def get_valueset(self) -> _Set[VT]:
        s: set = set()

        for v in self.__map.values():
            s.add(v)

        return s

    def get(self, k: KT):
        return self.__map.get(k)

    def __contains__(self, o: object) -> bool:
        return self.__map.__contains__(o)


if __name__ == '__main__':
    map_ = Map({"Hallo": 300, "Hoi": 200})
    print(map_.get_entryset())
