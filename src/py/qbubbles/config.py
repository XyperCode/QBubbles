import warnings
from pathlib import Path

import dill


class Reader(object):
    def __init__(self, config_file):
        warnings.warn("Call to deprecated class, use FileGameData(path).read() instead", DeprecationWarning)
        self.configFile = config_file

        with open(config_file, "rb") as file:
            data = dill.load(file)

        # file = NZTFile(config_file, "r")
        # file.load()
        # file.close()
        self.data = data

    def get_decoded(self):
        data = self.data
        return data


class Writer(object):
    def __init__(self, config_file, obj):
        warnings.warn("Call to deprecated class, use FileGameData(path).write() instead", DeprecationWarning)
        self.data = data = obj

        with open(config_file, "wb+") as file:
            dill.dump(config_file, file)

        # file = NZTFile(config_file, "w")
        # file.data = data
        # file.save()
        # file.close()


class FileConfiguration(object):
    def __init__(self, path: Path):
        self._path = path

    def write(self, data):
        from yaml import safe_dump
        with open(str(self._path.absolute()), "w+") as file:
            safe_dump(data, file)
            file.close()

    def read(self):
        from yaml import safe_load
        with open(str(self._path.absolute()), "r") as file:
            data = safe_load(file)
            file.close()
        return data


class FileGameData(object):
    def __init__(self, path: Path):
        self._path = path

    def write(self, data):
        from dill import dump
        with open(str(self._path.absolute()), "wb+") as file:
            dump(data, file)
            file.close()

    def read(self):
        from dill import load
        with open(str(self._path.absolute()), "rb") as file:
            data = load(file)
            file.close()
        return data
