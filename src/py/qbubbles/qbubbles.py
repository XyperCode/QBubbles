import qbubbles.gameScene
import qbubbles.registries


# noinspection PyUnusedClass,PyUnusedFunction
class QBubbles(object):
    @classmethod
    def get_game_scene(cls) -> qbubbles.gameScene.GameScene:
        return qbubbles.registries.Scenes.get("qbubbles:game")

    @classmethod
    def get_registry(cls, name) -> qbubbles.registries.Registry:
        if name in dir(qbubbles.registries):
            registry = getattr(qbubbles.registries, name)
            if registry == qbubbles.registries.Registry:
                raise PermissionError("Can't get base registry")
            return registry
        else:
            raise NameError(rf"No internal registry found with the name {repr(name)}")
