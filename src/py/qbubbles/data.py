import qbubbles.exceptions


class SaveData(dict):
    INSTANCE: 'SaveData'

    def __init__(self):
        if hasattr(SaveData, "INSTANCE"):
            raise qbubbles.exceptions.IllegalStateError("SaveData instance already created!")

        dict.__init__({})

        SaveData.INSTANCE = self


SAVE_DATA = SaveData()
