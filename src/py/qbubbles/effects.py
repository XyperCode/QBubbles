import time as _time
import typing as _t

# import qbubbles.entities
# import qbubbles.entities
from abc import ABC

import qbubbles.events as _evts
import qbubbles.entities as entities
import qbubbles.util as util


class BaseEffect(util.Registrable, ABC):
    def __init__(self):
        """
        Base effect class constructor.
        """

        self.__key: _t.Optional[util.NamespacedKey] = None
        self.callWhen: _t.Callable[[object, float, _t.Union[float, int]], bool] = lambda game, time, strength: True
        self.incompatibles: _t.List[BaseEffect] = []

    def on_apply(self, effect: 'AppliedEffect', entity):
        """
        Applief-effect start event handler.

        :param effect:
        :param entity:
        :return:
        """

        pass

    def on_update(self, effect: 'AppliedEffect', evt: _evts.UpdateEvent):
        pass

    def on_stop(self, effect: 'AppliedEffect', entity):
        """
        Applied-effect stop event handler.

        :param effect:
        :param entity:
        :return:
        """

        pass

    def __call__(self, game, time, strength, entity) -> _t.Optional['AppliedEffect']:
        """
        Used for getting the applied effect for the entity, featuring event handling and remaining time.

        :param game: The Game Scene
        :param time: The amount of time the effect shold be active
        :param strength: The strength of the effect
        :return: AppliedEffect instance or None if the BaseEffect callWhen call returns False.
        """
        if not self.callWhen(game, time, strength):
            return

        return AppliedEffect(self, game, time, strength, entity)

    def set_uname(self, name):
        """
        Sets the unlocalized of the base-effect..

        :param name: The unlocalized name to set.
        :return:
        """

        # for symbol in name:
        #     if symbol not in _str.ascii_letters+_str.digits+ "_:":
        #         raise _exc.UnlocalizedNameError(f"Invalid character '{symbol}' for unlocalized name '{name}'")
        
        self.__key = util.NamespacedKey.from_key(name)
        # if name[0] not in _str.ascii_letters:
        #     raise _exc.UnlocalizedNameError(f"Invalid start character '{name[0]}' for unlocalized name '{name}'")
        # if name[-1] not in _str.ascii_letters+_str.digits:
        #     raise _exc.UnlocalizedNameError(f"Invalid start character '{name[-1]}' for unlocalized name '{name}'")
        #
        #     raise ValueError(f"Effect '{self.__class__.__module__}.{self.__class__.__name__}' has already an unlocalized name")
        # if self in _g.NAME2EFFECT.values():
        #     raise ValueError(f"Effect '{self.__class__.__module__}.{self.__class__.__name__}' has already an unlocalized name")
        # if name in _g.EFFECT2NAME.values():
        #     raise ValueError(f"Name '{name}' already defined for effect '{self.__class__.__module__}.{self.__class__.__name__}'")
        # if name in _g.NAME2EFFECT.keys():
        #     raise ValueError(f"Name '{name}' already defined for effect '{self.__class__.__module__}.{self.__class__.__name__}'")

        return self.get_key()

    def __repr__(self):
        return f"<Effect({self.get_key()})>"

    def get_key(self):
        """
        Gets the unlocalized name of the base-effect.

        :returns: The unlocalized name of the base-effect.
        """

        return self.__key


class AppliedEffect(object):
    def __init__(self, baseclass: BaseEffect, game, duration: float, strength: _t.Union[float, int],
                 entity, **extradata):
        """
        Applied effect, is an effect that is applied to an entity / entity like a player or a bubble.

        :param baseclass: The BaseEffect()-instance. Used for ID and events.
        :param game: The Game-Scene instance
        :param duration: The effect duration.
        :param strength: The effect strength.
        :param extradata: The extra data to add to the effect.
        """

        self.baseObject: BaseEffect = baseclass
        self.baseUname: util.NamespacedKey = baseclass.get_key()

        active_effects = [applied_effect.baseObject for applied_effect in game.gameType.player.appliedEffects]
        for base_effect in self.baseObject.incompatibles:
            if base_effect in active_effects:
                return
            if self.baseObject.__class__ in base_effect.incompatibles:
                return

        self.strength = strength
        self.extraData = extradata
        self._game = game
        self.entity = entity

        self.boundEntity = entity

        self._pause = False
        self.dead = False
        self.pause_duration: _t.Optional[float] = None

        # if duration < 0:
        #     self.dead = True

        self._endTime = _time.time() + duration

        self.on_apply(entity)

    def __repr__(self):
        return f"AppliefEffect(<{self.baseObject.get_uname()}>, {self.get_remaining_time()}, {self.strength})"

    def get_end_time(self):
        """
        Gets the end time of the effect, can be changed using set_remaining_time().

        :returns: The end time of the effect
        """

        return self._endTime

    def on_pause(self, evt: _evts.PauseEvent):
        self._pause = evt.pause

        if evt.pause is True:
            self.pause_duration = self.get_remaining_time()

    def get_data(self):
        return {"id": self.baseUname, "duration": self.get_remaining_time(), "strength": self.strength, "extradata": list(self.extraData.items())}

    def on_stop(self, entity):
        self.baseObject.on_stop(self, entity)
        _evts.PauseEvent.unbind(self.on_pause)
        _evts.UpdateEvent.unbind(self.on_update)

    def on_apply(self, entity):
        self.baseObject.on_apply(self, entity)
        _evts.PauseEvent.bind(self.on_pause)
        _evts.UpdateEvent.bind(self.on_update)
        _evts.EffectApplyEvent(self._game, self._game.canvas, self)

    def on_update(self, evt: _evts.UpdateEvent):
        # Logging.debug("AppliedEffect", f"UpdateEvent: {self.on_update}")

        if self._pause:
            # print(self.pause_duration)
            self.set_remaining_time(self.pause_duration)
        elif self.get_remaining_time() < 0:
            self.dead = True
            self.on_stop(self.entity)
        else:
            self.baseObject.on_update(self, evt)

        # self.on_stop(self.entity)

    def get_key(self):
        """
        Gets the namespaced key of the base-effect.

        @return: Namespaced Key.
        """

        return self.baseObject.get_key()

    def get_remaining_time(self) -> float:
        """
        Gets the remaining time of the effect.

        :return:
        """

        return self._endTime - _time.time()

    def set_remaining_time(self, time_length: float):
        """
        Sets the remaining time of the effect.

        :param time_length:
        :return:
        """

        self._endTime = _time.time() + time_length


class SpeedBoostEffect(BaseEffect):
    def __init__(self):
        super(SpeedBoostEffect, self).__init__()

        self.set_uname("speedboost")

    def on_apply(self, effect: 'AppliedEffect', entity):
        entity.speed += (effect.strength * entity.baseSpeed) / 4

    def on_stop(self, effect: 'AppliedEffect', entity):
        entity.speed -= (effect.strength * entity.baseSpeed) / 4


class DefenceBoostEffect(BaseEffect):
    def __init__(self):
        super(DefenceBoostEffect, self).__init__()

    def on_apply(self, effect: 'AppliedEffect', entity):
        entity.defenceMultiplier += effect.strength * 2

    def on_stop(self, effect: 'AppliedEffect', entity):
        entity.defenceMultiplier -= effect.strength * 2


class ScoreMultiplierEffect(BaseEffect):
    def __init__(self):
        super(ScoreMultiplierEffect, self).__init__()

        self.set_uname("score_multiplier")

    def on_apply(self, effect: 'AppliedEffect', entity):
        if entity.get_key() == "player":
            entity: entities.Player
            entity.scoreMultiplier += effect.strength

    def on_stop(self, effect: 'AppliedEffect', entity):
        if entity.get_key() == "player":
            entity: entities.Player
            entity.scoreMultiplier -= effect.strength
