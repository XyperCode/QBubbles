import inspect
import warnings
import qbubbles
from abc import ABC, abstractmethod
from pathlib import Path
from tkinter import Tk, PhotoImage, Toplevel
from typing import Callable, Type, List, Union, Dict, Any, Tuple

from PIL import ImageTk
from overload import overload

from qbubbles.exceptions import UnlocalizedNameError, DuplicateAddonError, IllegalStateError, OverrideWarning, \
    TypeWarning
from qbubbles.gameIO import printerr, printwrn
from qbubbles.util import NamespacedKey

NamespacedString = Union[NamespacedKey, str]


# noinspection PyTypeChecker,PyUnusedLocal,PyUnusedName
class Registry(object):
    saveConfig = {}
    saveData = {}
    gameConfig = {}
    gameData = {}

    _registryAbilities = {}
    _registryGameTypes = {}
    _registryEffects = {}
    _registryScenes = {}
    _registrySceneManager = None
    _registryModes = {}
    _registryModeManager = None
    _registryBubbles = {}
    _registryEntities = {}
    _registryKeyBinds = {}
    _registryXboxBinds = {}
    _registryImages = {}
    _registryForegrounds = {}
    _registryBackgrounds = {}
    _registryIcons = {}
    _registryStoreIcons = {}
    _registryBubResources = {}
    _registryDefaultTextures = {}
    _registryTextures = {}
    _registryAddonPath = {}
    _registryAddons = {}
    _registryRoot: Dict[str, Union[Tk, Toplevel]] = {}

    @classmethod
    def get_abilities(cls):
        return [ability for ability in cls._registryAbilities.values()]

    @classmethod
    def ability_exists(cls, namespace):
        return namespace in cls._registryAbilities.keys()

    @classmethod
    def register_ability(cls, namespace: NamespacedKey, ability):
        if type(namespace) != NamespacedKey:
            raise TypeError(f"Ability uname must be a NamespacedKey-object not "
                            f"{'an' if namespace.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{namespace.__class__.__name__}-object")
        cls._registryAbilities[namespace] = ability

    @classmethod
    def get_entities(cls):
        return [entity for entity in cls._registryEntities.values()]

    @classmethod
    def entity_exists(cls, sname):
        return sname in cls._registryEntities.keys()

    @classmethod
    def register_entity(cls, sname, entity):
        if type(sname) != str:
            raise TypeError(f"Entity sname must be a str-object not "
                            f"{'an' if sname.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{sname.__class__.__name__}-object")
        cls._registryEntities[sname] = entity

    @classmethod
    def get_gametype(cls, uname):
        return cls._registryGameTypes[uname]

    @classmethod
    def get_gametypes(cls):
        return [uname for uname in cls._registryGameTypes.keys()]

    @classmethod
    def get_gametype_objects(cls):
        return [game_type for game_type in cls._registryGameTypes.values()]

    @classmethod
    def gametype_exists(cls, uname):
        return uname in cls._registryGameTypes.keys()

    @classmethod
    def register_gametype(cls, uname, game_type):
        if type(uname) != str:
            raise TypeError(f"GameType uname must be a str-object not "
                            f"{'an' if uname.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{uname.__class__.__name__}-object")
        # printerr(f"{repr(game_type)} is not a GameType Representation")
        if not (repr(game_type).startswith("GameType<") and repr(game_type).endswith(">")):
            raise ValueError(
                f"Representation is not of a GameType-object, or is this not a subclass of a GameType-object?")
        cls._registryGameTypes[uname] = game_type

    @classmethod
    def get_effect(cls, uname):
        return cls._registryEffects[uname]

    @classmethod
    def effect_exists(cls, uname):
        return uname in cls._registryEffects.keys()

    @classmethod
    def register_effect(cls, uname, effect):
        if type(uname) != str:
            raise TypeError(f"Effect uname must be a str-object not "
                            f"{'an' if uname.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{uname.__class__.__name__}-object")
        # printerr(f"{repr(effect)} is not a EffectObject Representation")
        if not (repr(effect).startswith("EffectObject(<") and repr(effect).endswith(">)")):
            raise ValueError(f"Representation is not of a Effect-object, or is this not a subclass of a Effect-object?")
        cls._registryEffects[uname] = effect

    # noinspection PySameParameterValue
    @classmethod
    def get_scene(cls, name):
        return cls._registryScenes[name]

    @classmethod
    def scene_exists(cls, name):
        return name in cls._registryScenes.keys()

    @classmethod
    def get_mode(cls, name):
        return cls._registryModes[name]

    @classmethod
    def mode_exists(cls, name):
        return name in cls._registryModes.keys()

    @classmethod
    def get_bubble(cls, id_):
        return cls._registryBubbles[id_]

    @classmethod
    def get_keybinding(cls, key):
        return cls._registryKeyBinds[key]

    @classmethod
    def get_xboxbinding(cls, key):
        return cls._registryXboxBinds[key]

    @classmethod
    def get_window(cls, name: str) -> Union[Tk, Toplevel]:
        return cls._registryRoot[name]

    @classmethod
    def get_id_bubble(cls, bubble) -> List[str]:
        return [key for key, value in cls._registryBubbles if value == bubble]

    @classmethod
    def register_scene(cls, name: str, scene: Type[Callable]):
        if type(name) != str:
            raise TypeError(f"Scene name must be a str-object not "
                            f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{name.__class__.__name__}-object")
        # printerr(f"{repr(scene)} is not a SceneObject Representation")
        if not (repr(scene).startswith("SceneObject<") and repr(scene).endswith(">")):
            raise ValueError(f"Representation is not of a Scene-object, or is this not a subclass of a Scene-object?")
        cls._registryScenes[name] = scene

    @classmethod
    def register_mode(cls, name: str, mode: object):
        if type(name) != str:
            raise TypeError(f"Mode name must be a str-object not "
                            f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{name.__class__.__name__}-object")
        if not (repr(mode).startswith("ModeObject<") and repr(mode).endswith(">")):
            raise ValueError(f"Representation is not of a Mode-object, or is this not a subclass of a Mode-object?")
        cls._registryModes[name] = mode

    @classmethod
    def register_bubble(cls, id_: str, bubbleobj: object):
        if type(id_) != str:
            raise TypeError(f"Bubble ID must be a str-object not "
                            f"{'an' if id_.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{id_.__class__.__name__}-object")
        if not (repr(bubbleobj).startswith("Bubble(<") and repr(bubbleobj).endswith(">)")):
            raise ValueError(f"Representation is not of a Bubble-object, or is this not a subclass of a Bubble-object?")
        cls._registryBubbles[id_] = bubbleobj

    @classmethod
    def register_keybinding(cls, key: str, command: Callable):
        if type(key) != str:
            raise TypeError(f"Key-binding must be a str-object not "
                            f"{'an' if key.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                            f"{key.__class__.__name__}-object")

        if key in cls._registryXboxBinds:
            printerr(f"Key-binding with key '{key}' already exists!")
        cls._registryKeyBinds[key] = command

    @classmethod
    def register_xboxbinding(cls, key: str, command: Callable):
        if type(key) != str:
            raise TypeError(
                f"Xbox-binding must be a str-object not "
                f"{'an' if key.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{key.__class__.__name__}-object")

        if key in cls._registryXboxBinds:
            printerr(f"Xbox-binding with key '{key}' already exists!")
        cls._registryXboxBinds[key] = command

    @classmethod
    def register_window(cls, name, window):
        if not issubclass(type(window), Tk):
            if not issubclass(type(window), Toplevel):
                raise TypeError(
                    f"Window must be a subclass of Tk(...) or Toplevel(...) not "
                    f"{'an' if window.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{window.__class__.__name__}-object, which is not subclass of it")

        if name in cls._registryRoot.keys():
            printerr(f"Window with name '{name}' already exists!")
        cls._registryRoot[name] = window

    @classmethod
    def register_image(cls, name, image: PhotoImage, **data):
        if type(name) != str:
            raise TypeError(
                f"Image name must be a str-object not "
                f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{name.__class__.__name__}-object")
        if type(image) != PhotoImage:
            if type(image) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Image must be a PhotoImage-object not "
                    f"{'an' if image.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{image.__class__.__name__}-object")

        if name in cls._registryImages.keys():
            printerr(f"Image with name '{name}' already exists!")
        cls._registryImages[name] = {}
        cls._registryImages[name][tuple(data.items())] = image

    @classmethod
    def get_image(cls, name):
        if name not in cls._registryImages.keys():
            raise UnlocalizedNameError(f"image with name '{name}' is non-existent")
        icon = cls._registryImages[name]
        return icon

    @classmethod
    def register_foreground(cls, name, image: PhotoImage):
        if type(name) != str:
            raise TypeError(
                f"Foreground name must be a str-object not "
                f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{name.__class__.__name__}-object")
        if type(image) != PhotoImage:
            if type(image) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Foreground image must be a PhotoImage-object not "
                    f"{'an' if image.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{image.__class__.__name__}-object")

        if name in cls._registryXboxBinds:
            printerr(f"Foreground with name '{name}' already exists!")
        cls._registryForegrounds[name] = image

    @classmethod
    def get_foreground(cls, name):
        if name not in cls._registryForegrounds.keys():
            raise UnlocalizedNameError(f"foreground with name '{name}' is non-existent")
        icon = cls._registryForegrounds[name]
        return icon

    @classmethod
    def register_background(cls, name, image: PhotoImage):
        if type(name) != str:
            raise TypeError(
                f"Background name must be a str-object not "
                f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{name.__class__.__name__}-object")
        if type(image) != PhotoImage:
            if type(image) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Background image must be a PhotoImage-object not "
                    f"{'an' if image.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{image.__class__.__name__}-object")

        if name in cls._registryXboxBinds:
            printerr(f"Image with name '{name}' already exists!")
        cls._registryBackgrounds[name] = image

    @classmethod
    def get_background(cls, name):
        if name not in cls._registryBackgrounds.keys():
            raise UnlocalizedNameError(f"background with name '{name}' is non-existent")
        icon = cls._registryBackgrounds[name]
        return icon

    @classmethod
    def register_storeitem(cls, name, icon: PhotoImage, obj: Callable = None):
        if type(name) != str:
            raise TypeError(
                f"Store item name must be a str-object not "
                f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{name.__class__.__name__}-object")
        if type(icon) != PhotoImage:
            if type(icon) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Store item icon must be a PhotoImage-object not "
                    f"{'an' if icon.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{icon.__class__.__name__}-object")

        # # TODO: Make store item objects compatible
        # # printerr(f"{repr(scene)} is not a SceneObject Representation")
        # if not (repr(obj).startswith("StoreItem<") and repr(obj).endswith(">")):
        #     raise ValueError(
        #         f"Representation is not of a StoreItem-object, or is this not a subclass of a Scene-object?")
        if name in cls._registryXboxBinds:
            printerr(f"Store item icon with name '{name}' already exists!")
        cls._registryStoreIcons[name] = icon

    @classmethod
    def register_icon(cls, name, image: PhotoImage):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if type(name) != str:
            raise TypeError(
                f"Icon name must be a str-object not "
                f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{name.__class__.__name__}-object")
        if type(image) != PhotoImage:
            if type(image) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Icon image must be a PhotoImage-object not "
                    f"{'an' if name.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{image.__class__.__name__}-object")

        if name in cls._registryXboxBinds:
            printerr(f"Icon with name '{name}' already exists!")
        cls._registryIcons[name] = image

    @classmethod
    def get_icon(cls, name):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if name not in cls._registryIcons.keys():
            raise NameError(f"icon with name '{name}' is non-existent")
        icon = cls._registryIcons[name]
        return icon

    @classmethod
    def get_bubresource(cls, uname, key):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if uname not in cls._registryBubResources.keys():
            raise UnlocalizedNameError(f"bubble resource with uname '{uname}' is non-existent")
        bub_reslist = cls._registryBubResources[uname]
        if key not in bub_reslist.keys():
            raise KeyError(f"key '{key}' for bubble resource '{uname}' is non-existent")
        bub_res = bub_reslist[key]
        return bub_res

    @classmethod
    def bubresource_exists(cls, uname, key=None):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if uname in cls._registryBubResources.keys():
            if key is not None:
                return key in cls._registryBubResources[uname].keys()
            return True
        return False

    @classmethod
    def register_bubresource(cls, uname, key, value):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if type(uname) != str:
            raise TypeError(
                f"uname for bubble resource must be a str-object not "
                f"{'an' if uname.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{uname.__class__.__name__}-object")
        if type(key) != str:
            raise TypeError(
                f"key for bubble resource must be a str-object not "
                f"{'an' if key.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{key.__class__.__name__}-object")

        if uname in cls._registryBubResources.keys():
            if key in cls._registryBubResources[uname].keys():
                printwrn(f"bubble resource key '{key}' with uname '{uname}' is overridden, "
                         f"this can be a intended override, but usually a mistake")
            cls._registryBubResources[uname][key] = value
        else:
            cls._registryBubResources[uname] = {key: value}

    @classmethod
    def get_dtexture(cls, type_, id_) -> Union[PhotoImage, ImageTk.PhotoImage]:
        warnings.warn("Call to deprecated method", DeprecationWarning)
        # if "qbubbles:default" not in cls._registryDefaultTextures.keys():
        #     raise RuntimeError("Missing type 'qbubbles:default'")
        if type_ not in cls._registryDefaultTextures.keys():
            # cls.get_scenemanager().change_scene("qbubbles:error_scene",
            #                                     f"Game Crashed",
            #                                     f"Missing default texture for type '{type_}'")
            raise RuntimeError(f"Missing default texture for type '{type_}'")
        if id_ not in cls._registryDefaultTextures[type_].keys():
            return cls._registryDefaultTextures[type_][None]
        return cls._registryDefaultTextures[type_][id_]

    @classmethod
    def register_default_texture(cls, type_: str, id_: str = None, *, texture):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if id_ is None:
            if type_ not in cls._registryDefaultTextures.keys():
                cls._registryDefaultTextures[type_] = {}
            else:
                printwrn(f"Default texture of type '{type_}' has been overridden")
            cls._registryDefaultTextures[type_][None] = texture
        else:
            if type_ not in cls._registryDefaultTextures.keys():
                raise RuntimeError(f"There is no default texture registered for type '{type_}'")
            if id_ in cls._registryDefaultTextures[type_].keys():
                printwrn(f"Default texture for type '{type_}' with id '{id_}' has been overridden")
            cls._registryDefaultTextures[type_][id_] = texture

    @classmethod
    def get_texture(cls, type_, id_, **data) -> Union[PhotoImage, ImageTk.PhotoImage]:
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if type_ not in cls._registryTextures.keys():
            printerr(f"Type '{type_}' is not found in texture registry")
            return cls.get_dtexture(type_, id_)
        if id_ not in cls._registryTextures[type_].keys():
            printerr(f"ID '{id_}' with type '{type_}' is not found in texture registry")
            return cls.get_dtexture(type_, id_)
        if tuple(data.items()) not in cls._registryTextures[type_][id_]:
            printerr(f"ID '{id_}' with type '{type_}' and data '{dict(data)}' is not found in texture registry")
            return cls.get_dtexture(type_, id_)
        return cls._registryTextures[type_][id_][tuple(data.items())]

    @classmethod
    def get_current_scene(cls):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return cls.get_scene("Game").scenemanager.currentScene

    @classmethod
    def get_scenemanager(cls):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return cls.get_scene("Game").scenemanager

    @classmethod
    def register_scenemanager(cls, scenemanager):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if cls._registrySceneManager is not None:
            raise RuntimeError("scenemanager already registered")
        cls._registrySceneManager = scenemanager

    @classmethod
    def register_texture(cls, type_, id_, texture, **data):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if type(type_) != str:
            raise TypeError(
                f"Texture type must be a str-object not "
                f"{'an' if type_.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{type_.__class__.__name__}-object")
        if type(id_) != str:
            raise TypeError(
                f"Texture ID must be a str-object not "
                f"{'an' if id_.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{id_.__class__.__name__}-object")
        if type(texture) != PhotoImage:
            if type(texture) != ImageTk.PhotoImage:
                raise TypeError(
                    f"Image must be a PhotoImage-object not "
                    f"{'an' if texture.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{texture.__class__.__name__}-object")

        if type_ in cls._registryTextures.keys():
            if id_ in cls._registryTextures[type_].keys():
                if tuple(data.items()) in cls._registryTextures[type_][id_]:
                    printwrn(f"texture with ID '{id_}' and type '{type_}' is overridden, "
                             f"this can be a intended override, but usually a mistake")
                cls._registryTextures[type_][id_][tuple(data.items())] = texture
            else:
                # cls._registryTextures[type_][id_] = texture
                cls._registryTextures[type_][id_] = {}
                cls._registryTextures[type_][id_][tuple(data.items())] = texture
        else:
            cls._registryTextures[type_] = {id_: {tuple(data.items()): texture}}

    @classmethod
    def get_bubbles(cls):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return tuple(cls._registryBubbles.values())

    @classmethod
    def get_lname(cls, *args):
        warnings.warn("Call to deprecated method", DeprecationWarning)

        l_id = ".".join([*args])
        if l_id in cls.gameData["language"].keys():
            return cls.gameData["language"][l_id]
        else:
            return l_id

    @classmethod
    def register_entity_image(cls, name, image, **data):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        # pass

    @classmethod
    def register_addon(cls, addonid: str, name: str, version: str, clazz: object):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        # print(addonid, name, version, func)
        path = inspect.getfile(clazz)
        if addonid in cls._registryAddons.keys():
            raise DuplicateAddonError(addonid, name, version, path, clazz)
        cls._registryAddonPath[Path(path).absolute()] = addonid
        cls._registryAddons[addonid] = dict(name=name, version=version, addon=clazz)

    @classmethod
    def get_all_addons(cls):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return [modid for modid in cls._registryAddons.keys()]

    @classmethod
    def mod_exists(cls, modid, version=None):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        if version is not None:
            if modid in cls._registryAddons.keys():
                if version.split(".") in cls._registryAddons[modid]["version"].split("."):
                    return True
            return False
        return modid in cls._registryAddons.keys()

    @classmethod
    def get_module(cls, modid):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return cls._registryAddons[modid]

    @classmethod
    def get_entity(cls, uname):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return cls._registryEntities[uname]

    @classmethod
    def get_addonid_by_path(cls, path: Path):
        warnings.warn("Call to deprecated method", DeprecationWarning)
        return Registry._registryAddonPath[path]


# noinspection PyRedeclaration
class Registry(ABC):
    __registry: Dict[str, Any] = {}

    @classmethod
    def _check_type(cls, o: Any, types: List[Type]):
        b = False
        for type_ in types:
            if isinstance(o, type_):
                b = True

        if not b:
            raise TypeError(rf"Class is not subclassed from one of the required type(s): "
                            rf"{', '.join(type_.__name__ for type_ in types)}")

    @classmethod
    def _check_repr(cls, clazz, name):
        b = repr(clazz).startswith("<" + name + "(") and repr(clazz).endswith(")>")
        if not b:
            raise TypeError(rf"Class have not the required repr {repr('<' + name + '(...)>')}, "
                            rf"maybe make a subclass of the type you need? {repr(clazz)}")

    @classmethod
    def _check_exists(cls, name, reg_dict):
        if name is None:
            warnings.warn(rf"No name specified", TypeWarning, stacklevel=3)
        b = name in tuple(reg_dict.keys())
        if b:
            warnings.warn(rf"Name {repr(name)} is overridden in registry, "
                          rf"this can be a intended override, but usually a mistake", OverrideWarning, stacklevel=3)

    @classmethod
    @abstractmethod
    def register(cls, clazz):
        return clazz

    @classmethod
    @abstractmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    @abstractmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @classmethod
    @abstractmethod
    @overload
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)]


# noinspection PyNestedDecorators
class Abilities(Registry):
    __registry: Dict[NamespacedKey, Any] = {}

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "Ability")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


# noinspection PyNestedDecorators
class Bubbles(Registry):
    __registry: Dict[NamespacedKey, Any] = {}

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "BubbleType")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


class Addons(Registry):
    __registry: Dict[str, 'qbubbles.addon.Addon'] = {}
    __registryPath = {}

    # noinspection PyMethodOverriding
    @classmethod
    def register(cls, clazz: 'qbubbles.addon.Addon', path):
        clazz = clazz  # type: qbubbles.addon.Addon
        cls._check_repr(clazz, "Addon")
        cls._check_exists(clazz.get_addonid(), cls.__registry)
        cls.__registry[clazz.get_addonid()] = clazz
        cls.__registryPath[path] = clazz.get_addonid()
        return clazz

    @classmethod
    def values(cls) -> Tuple['qbubbles.addon.Addon', ...]:
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @classmethod
    def paths(cls):
        return tuple(cls.__registryPath.keys())

    @classmethod
    def get(cls, name):
        if name == "qbubbles":
            return None

        return cls.__registry[name]

    @classmethod
    def get_addon_from_trace(cls):
        """
        Get addon from trace C{Trace}

        @return: C{qbubbles.addon.Addon}
        @rtype: qbubbles.addon.Addon
        @see: C{qbubbles.util.Trace}
        """

        from qbubbles import Trace
        from qbubbles import References

        # Frame
        frames = Trace.get_current().find_in_path(References.INSTANCE.addonsFolder)
        frame = frames[-1]
        frame: inspect.FrameInfo

        # Path
        filename = Path(
            Path(frame.filename).absolute().as_posix().split(References.INSTANCE.addonsFolder.absolute().as_posix(), 1)[
                1]).parts[0]
        path = References.INSTANCE.addonsFolder.joinpath(filename).absolute()

        # Addon
        addonid = cls.get_addonid_from_path(path)
        addon = cls.get(addonid)
        return addon

    @classmethod
    def get_addonid_from_path(cls, path: Path):
        return cls.__registryPath[path] if path in cls.paths() else None


# noinspection PyNestedDecorators
class Entities(Registry):
    __registry = {}

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "Entity")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


# noinspection PyNestedDecorators
class Effects(Registry):
    __registry = {}

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "Effect")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


# noinspection PyNestedDecorators
class GameTypes(Registry):
    __registry = {}

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "GameType")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


# noinspection PyNestedDecorators
class TypeRegistry(Registry):
    # __registry = {}
    __registryTransType = []
    __registryTranslators = {}

    @classmethod
    def register(cls, clazz):
        # cls._check_repr(clazz, "GameType")
        # cls._check_exists(clazz.get_key(), cls.__registry)
        # cls.__registry[clazz.get_key()] = clazz
        # return clazz
        raise PermissionError("Not allowed to register: no standard registry.")

    @classmethod
    def register_translation_type(cls, name, name_suffix=None, tag_suffix=None):
        cls.__registryTransType.append((name, dict(name=name_suffix, tag=tag_suffix)))

    @classmethod
    def register_translator(cls, lang, translator):
        cls._check_repr(translator, "Translator")
        cls._check_exists(lang, cls.__registryTranslators)
        cls.__registryTranslators[lang] = translator

    @classmethod
    def get_translator(cls, lang: str = None):
        if lang is None:
            lang = GAME_CONFIG["config"]["Game"]["language"]

        try:
            return cls.__registryTranslators[lang]
        except KeyError:
            raise NameError(rf"Translator with language ID {repr(lang)} was not found.") from None

    @classmethod
    def values(cls):
        raise RuntimeError("No standard registry.")

    @classmethod
    def keys(cls):
        raise RuntimeError("No standard registry.")

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        raise RuntimeError("No standard registry.")
        # return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        raise RuntimeError("No standard registry.")
        # return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


# noinspection PyNestedDecorators
class Textures(Registry):
    __registry = {}
    __registryDefault = {}

    # noinspection PyMethodOverriding
    @classmethod
    def register(cls, type_key: NamespacedKey, key: NamespacedKey, texture, **data):
        # cls._check_repr(clazz, "GameType")
        # cls._check_exists(clazz.get_key(), cls.__registry)
        # cls.__registry[clazz.get_key()] = clazz
        if type(type_key) != NamespacedKey:
            raise TypeError(
                f"Texture type must be a namespaced key not "
                f"{'an' if type_key.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{type_key.__class__.__name__}-object")
        if type(key) != NamespacedKey:
            raise TypeError(
                f"Texture ID must be a namespaced key not "
                f"{'an' if key.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                f"{key.__class__.__name__}-object")
        if not isinstance(texture, PhotoImage):
            if not isinstance(texture, ImageTk.PhotoImage):
                raise TypeError(
                    f"Image must be an instance of the PhotoImage-object from tkinter or Pillow.ImageTk not "
                    f"{'an' if texture.__class__.__name__.startswith(('e', 'a', 'i', 'o', 'u')) else 'a'} "
                    f"{texture.__class__.__name__}-object")

        if type_key in cls.__registry.keys():
            if key in cls.__registry[type_key].keys():
                if tuple(data.items()) in cls.__registry[type_key][key]:
                    warnings.warn(f"texture with key '{key}' and type '{type_key}' is overridden, "
                                  f"this can be a intended override, but usually a mistake",
                                  OverrideWarning, stacklevel=2)
                cls.__registry[type_key][key][tuple(data.items())] = texture
            else:
                # cls._registryTextures[type_][id_] = texture
                cls.__registry[type_key][key] = {}
                cls.__registry[type_key][key][tuple(data.items())] = texture
        else:
            cls.__registry[type_key] = {key: {tuple(data.items()): texture}}
        return texture

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    # @overload
    # @classmethod
    # def get(cls, key: NamespacedKey):
    #     return cls.__registry[key] if key in cls.keys() else None
    #
    # @classmethod
    # @get.add
    # def get(cls, key: str):
    #     return cls.__registry[NamespacedKey.from_string(key)] if
    #         NamespacedKey.from_string(key) in cls.keys() else None

    # noinspection PyMethodOverriding
    @classmethod
    def get(cls, type_key: NamespacedString, key: NamespacedString, **data) -> Union[PhotoImage, ImageTk.PhotoImage]:
        # print(cls.__registry)

        if isinstance(type_key, str):
            type_key: NamespacedKey = NamespacedKey.from_string(type_key)
        if isinstance(key, str):
            key: NamespacedKey = NamespacedKey.from_string(key)

        if type_key not in cls.__registry.keys():
            warnings.warn(
                f"Type key '{type_key}' is not found in texture registry",
                ResourceWarning, stacklevel=2)
            return cls.get_default(type_key, key)
        if key not in cls.__registry[type_key].keys():
            warnings.warn(
                f"Key '{key}' with type '{type_key}' is not found in texture registry",
                ResourceWarning, stacklevel=2)
            return cls.get_default(type_key, key)
        if tuple(data.items()) not in cls.__registry[type_key][key]:
            warnings.warn(
                f"Key '{key}' with type '{type_key}' and data '{dict(data)}' is not found in texture registry",
                ResourceWarning, stacklevel=2)
            return cls.get_default(type_key, key)
        return cls.__registry[type_key][key][tuple(data.items())]

    # noinspection PyUnusedLocal
    @classmethod
    def get_default(cls, type_: NamespacedKey, id_: NamespacedKey = None):
        # if "qbubbles:default" not in cls._registryDefaultTextures.keys():
        #     raise RuntimeError("Missing type 'qbubbles:default'")

        if type_ not in cls.__registryDefault.keys():
            # cls.get_scenemanager().change_scene("qbubbles:error_scene",
            #                                     f"Game Crashed",
            #                                     f"Missing default texture for type '{type_}'")
            raise RuntimeError(f"Missing default texture for type '{type_}'")
        if id_ not in cls.__registryDefault[type_].keys():
            return cls.__registryDefault[type_][None]
        return cls.__registryDefault[type_][id_]

    # noinspection PySameParameterValue
    @classmethod
    def register_default(cls, type_: NamespacedKey, id_: NamespacedKey = None, *, texture):
        if id_ is None:
            if type_ not in cls.__registryDefault.keys():
                cls.__registryDefault[type_] = {}
            else:
                printwrn(f"Default texture of type '{type_}' has been overridden")
            cls.__registryDefault[type_][None] = texture
        else:
            if type_ not in cls.__registryDefault.keys():
                raise RuntimeError(f"There is no default texture registered for type '{type_}'")
            if id_ in cls.__registryDefault[type_].keys():
                printwrn(f"Default texture for type '{type_}' with id '{id_}' has been overridden")
            cls.__registryDefault[type_][id_] = texture


# noinspection PySameParameterValue,PyNestedDecorators
class Scenes(Registry):
    __registry = {}
    __manager = None

    @classmethod
    def register(cls, clazz):
        cls._check_repr(clazz, "Scene")
        cls._check_exists(clazz.get_key(), cls.__registry)
        cls.__registry[clazz.get_key()] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None

    @classmethod
    def get_manager(cls):
        return cls.__manager

    @classmethod
    def set_manager(cls, manager):
        if cls.__manager is not None:
            warnings.warn("The scene manager is being overridden.", OverrideWarning, stacklevel=2)
        cls.__manager = manager


# noinspection PyNestedDecorators
class Windows(Registry):
    __registry = {}

    # noinspection PyMethodOverriding
    @classmethod
    def register(cls, clazz: Union[Tk, Toplevel], key: NamespacedKey):
        cls._check_type(clazz, [Tk, Toplevel])
        cls._check_exists(key, cls.__registry)
        cls.__registry[key] = clazz
        return clazz

    @classmethod
    def values(cls):
        return tuple(cls.__registry.values())

    @classmethod
    def keys(cls):
        return tuple(cls.__registry.keys())

    @overload
    @classmethod
    def get(cls, key: NamespacedKey):
        return cls.__registry[key] if key in cls.keys() else None

    @classmethod
    @get.add
    def get(cls, key: str):
        return cls.__registry[NamespacedKey.from_string(key)] if NamespacedKey.from_string(key) in cls.keys() else None


class GameConfig(dict):
    INSTANCE: 'GameConfig' = None

    def __init__(self):
        if GameConfig.INSTANCE is not None:
            raise IllegalStateError("GameConfig instance already created!")

        dict.__init__({})

        GameConfig.INSTANCE = self


class LauncherConfig(dict):
    INSTANCE: 'LauncherConfig' = None

    def __init__(self):
        if LauncherConfig.INSTANCE is not None:
            raise IllegalStateError("GameConfig instance already created!")

        dict.__init__({})

        LauncherConfig.INSTANCE = self


GAME_CONFIG = GameConfig()
LAUNCHER_CONFIG = LauncherConfig()
