import os
import warnings
from random import Random
from tkinter import Canvas
from typing import Dict, Tuple, Optional, List

import dill

from qbubbles import TimeLength
from qbubbles import BubbleSystem
from qbubbles import BubbleType, Bubble
from qbubbles import Reader
from qbubbles import Player
from qbubbles import UpdateEvent, CollisionEvent, KeyPressEvent, KeyReleaseEvent, XInputEvent, \
    MapInitializeEvent, FirstLoadEvent, SaveEvent, LoadCompleteEvent, GameExitEvent, PauseEvent, EffectApplyEvent
from qbubbles import printerr
from qbubbles import CPanel, CEffectBarArea
# from qbubbles.nzt import NZTFile
# from qbubbles.registry import Registry
# from qbubbles.entities import Entity, Player
from qbubbles import Font
import qbubbles as _gameIO

warnings.warn("Call to deprecated module, use gametypes instead.", DeprecationWarning)


class GameType(object):
    def __init__(self):
        self._bubbles = []
        self._gameobjects = []
        self.player: Optional[Player] = None
        self.seedRandom = None
        self.randoms: Dict[str, Tuple[Random, int]] = {}
        self._uname = None

    def load(self):
        MapInitializeEvent.bind(self.on_mapinit)
        FirstLoadEvent.bind(self.on_firstload)
        LoadCompleteEvent.bind(self.on_loadcomplete)

    def on_loadcomplete(self, evt: LoadCompleteEvent):
        pass

    def get_gameobjects(self) -> List[Entity]:
        return self._gameobjects

    @staticmethod
    def get_bubbles():
        return SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"]

    def create(self, seed, randoms=None):
        self.seedRandom = seed
        self.randoms: Dict[str, Tuple[Random, int]] = {}
        if randoms is not None:
            for random in randoms:
                randomState = random["State"]
                offset = random["offset"]
                id_ = random["id"]

                # noinspection PyNoneFunctionAssignment,PyTypeChecker
                r: Random = Random(self.seedRandom << offset).setstate(randomState)
                self.randoms[id_] = (r, offset)
        else:
            self.init_defaults()
        self._uname = None

    def add_random(self, id_, offset):
        if id_.count(":") != 1:
            printerr(f"Randomizer id must contain a single COLON, id: {id_}")
        self.randoms[id_] = self.format_random(offset)

    def init_defaults(self):
        self.add_random("qbubbles:effect.duration", 24)
        self.add_random("qbubbles:effect.strength", 16)
        self.add_random("qbubbles:bubblesystem", 4096)
        self.add_random("qbubbles:bubble.radius", 32)
        self.add_random("qbubbles:bubble.speed", 64)
        self.add_random("qbubbles:bubble.x", 128)
        self.add_random("qbubbles:bubble.y", 256)

    def on_mapinit(self, evt: MapInitializeEvent):
        pass

    def on_firstload(self, evt: FirstLoadEvent):
        pass

    def format_random(self, offset):
        if offset % 4 == 0:
            return Random(self.seedRandom << offset), offset
        else:
            raise ValueError("Offset must be multiple of 4")

    def __setattr__(self, key, value):
        if key == "format_random":
            if value != self.format_random:
                raise PermissionError("Cannot set format_random")
        self.__dict__[key] = value

    def set_uname(self, uname):
        self._uname = uname

    def get_uname(self):
        return self._uname

    def get_save_data(self):
        randoms = []
        for id, data in self.randoms.items():
            sdata = {}
            random: Random = data[0]
            sdata["State"] = random.getstate()
            sdata["offset"] = data[1]
            sdata["id"] = id
            randoms.append(sdata)

    def create_random_bubble(self, *, x=None, y=None):
        bubbleObject = self.get_random_bubble()
        w = GAME_CONFIG["WindowWidth"]
        h = GAME_CONFIG["WindowHeight"]

        if x is None:
            x = self.randoms["qbubbles:bubble.x"][0].randint(0 - radius, w + radius)
        if y is None:
            y = self.randoms["qbubbles:bubble.y"][0].randint(0 - radius, h + radius)
        self.create_bubble(x, y, bubbleObject)

    def get_random_bubble(self) -> Bubble:
        bubble: BubbleType = BubbleSystem.random(self.randoms["qbubbles:bubblesystem"][0])
        radius = self.randoms["qbubbles:bubble.radius"][0].randint(bubble.minRadius, bubble.maxRadius)
        speed = self.randoms["qbubbles:bubble.speed"][0].randint(bubble.minSpeed, bubble.maxSpeed)

        max_health = 1
        if hasattr(bubble, "maxHealth"):
            max_health = bubble.maxHealth

        return Bubble(
            bubble, max_health, max_health, radius=radius, speed=speed, health=bubble.hardness,
            scoremp=bubble.scoreMultiplier, attackmp=bubble.attackMultiplier, defencemp=bubble.defenceMultiplier)

    def create_bubble(self, x: int, y: int, bubble_object: Bubble):
        assert type(bubble_object) != type

        bubble_object.create(x=x, y=y)
        self._bubbles.append(bubble_object)
        self._gameobjects.append(bubble_object)

    def delete_bubble(self, bubble_object: Bubble):
        self._bubbles.remove(bubble_object)
        self._gameobjects.remove(bubble_object) if bubble_object in self._gameobjects else None
        bubble_object.delete()

    def on_update(self, evt: UpdateEvent):
        pass

    # def on_playermotion(self, evt: PlayerMotionEvent):
    #     pass

    def on_collision(self, evt: CollisionEvent):
        pass

    def on_keypress(self, evt: KeyPressEvent):
        pass

    def on_keyrelease(self, evt: KeyReleaseEvent):
        pass

    def on_xinput(self, evt: XInputEvent):
        pass

    def __repr__(self):
        return f"GameType<{self.get_uname()}>"

    def load_savedata(self, path):
        raise RuntimeError("Default Game Map does not support loading savedata")

    def save_savedata(self, path):
        raise RuntimeError("Default Game Map does not support saving savedata")

    def create_savedata(self, path, seed):
        raise RuntimeError("Default Game Map does not support creating savedata")


class GameMap(GameType):
    def __init__(self):
        super().__init__()
        warnings.warn("Call to deprecated class, use GameType instead.")

    def __getattribute__(self, key):
        warnings.warn("Call to deprecated class, use GameType instead.", DeprecationWarning)
        return self.__dict__[key]

    def __getattr__(self, key):
        warnings.warn("Call to deprecated class, use GameType instead.", DeprecationWarning)
        return self.__dict__[key]

    def __setattr__(self, key, value):
        warnings.warn("Call to deprecated class, use GameType instead.", DeprecationWarning)
        self.__dict__[key] = value


class ClassicMap(GameType):
    def __init__(self):
        super(ClassicMap, self).__init__()

        self._pause = False
        self.set_uname("qbubbles:classic_map")
        self.maxBubbles = 100
        self.texts = {}
        self.panelTop: Optional[CPanel] = None
        self.tSpecialColor = "#ffffff"
        self.tNormalColor = "#3fffff"
        self.effectImages = {}
        self.effectX = 100

    def init_defaults(self):
        self.add_random("qbubbles:effect.duration", 24)
        self.add_random("qbubbles:effect.strength", 16)
        self.add_random("qbubbles:bubblesystem", 4096)
        self.add_random("qbubbles:bubblesystem.start_x", 8)
        self.add_random("qbubbles:bubblesystem.start_y", 12)
        self.add_random("qbubbles:bubble.radius", 32)
        self.add_random("qbubbles:bubble.speed", 64)
        self.add_random("qbubbles:bubble.x", 128)
        self.add_random("qbubbles:bubble.y", 256)

    def on_effect_apply(self, evt: EffectApplyEvent):
        if evt.entity == self.player:
            self.effectX += 256
            lname = Registry.get_lname("effect", evt.appliedEffect.get_key().replace(":", "."), "name")
            name = GAME_CONFIG["language"][lname] if lname in GAME_CONFIG["language"].keys() else lname
            self.effectImages[evt.appliedEffect] = {
                # Textures
                "effectBar": self.canvas.create_image(
                    self.effectX, 5,
                    image=Registry.get_texture(
                        "gui", "qbubbles:effect_bar", gametype=self.get_uname()), anchor="nw"
                ),
                "icon": self.canvas.create_image(
                    self.effectX+1, 6,
                    image=Registry.get_texture(
                        "effect", evt.appliedEffect.get_key(), gametype=self.get_uname()), anchor="nw"
                ),

                # Texts
                "text": self.canvas.create_text(
                    self.effectX + 36, 18,
                    text=name % {
                        "strength": int(evt.appliedEffect.strength)
                    }, anchor="w"
                ),
                "time": self.canvas.create_text(
                    self.effectX + 200, 18, text=str(TimeLength(evt.appliedEffect.get_remaining_time())), anchor="w"
                )
            }
        
    def on_firstload(self, evt: FirstLoadEvent):
        _gameIO.Logging.info("Gamemap", "Create bubbles because the save is loaded for first time")

        w = GAME_CONFIG["WindowWidth"]
        h = GAME_CONFIG["WindowHeight"]
        for i in range(self.maxBubbles):
            self.create_random_bubble()
        SAVE_DATA["Game"]["GameType"]["initialized"] = True
        self.player.teleport(GAME_CONFIG["MiddleX"], GAME_CONFIG["MiddleY"])

    def on_mapinit(self, evt: MapInitializeEvent):
        w = GAME_CONFIG["WindowWidth"]
        h = GAME_CONFIG["WindowHeight"]

        self.seedRandom = SAVE_DATA["Game"]["GameType"]["seed"]
        self.init_defaults()

        canvas: Canvas = evt.canvas

        t1 = evt.t1
        t2 = evt.t2

        # noinspection PyUnusedLocal
        self.panelTop = CPanel(canvas, 0, 0, width="extend", height=69, fill="darkcyan", outline="darkcyan")
        # print(f"Panel Top created: {self.panelTop}")
        panelTopFont = Font("Helvetica", 12)

        # # Initializing the panels for the game.
        # self.panels["game/top"] = canvas.create_rectangle(
        #     -1, -1, GAME_CONFIG["WindowWidth"], 69, fill="darkcyan"
        # )

        # Create seperating lines.
        canvas.create_line(0, 70, GAME_CONFIG["WindowWidth"], 70, fill="lightblue")
        canvas.create_line(0, 69, GAME_CONFIG["WindowWidth"], 69, fill="lightblue")

        canvas.create_text(
            55, 30, text=Registry.get_lname("info", "score"),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Score")
        canvas.create_text(
            110, 30, text=Registry.get_lname("info", "level"),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Level")
        canvas.create_text(
            165, 30, text=Registry.get_lname("info", "speed"),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Speed")
        canvas.create_text(
            220, 30, text=Registry.get_lname("info", "lives"),
            fill=self.tSpecialColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Lives")

        CEffectBarArea(canvas, gametype=self)

        canvas.create_text(1120, 30, text=GAME_CONFIG["language"]["info.tps"],
                           fill=self.tNormalColor, font=panelTopFont.get_tuple())
        canvas.itemconfig(t2, text="Teleports")

        # Coin / Diamond icons
        canvas.create_image(1185, 30, image=Registry.get_icon("StoreDiamond"))
        canvas.itemconfig(t2, text="Diamonds")
        canvas.create_image(1185, 50, image=Registry.get_icon("StoreCoin"))
        canvas.itemconfig(t2, text="Coins")

        canvas.itemconfig(t1, text="Creating Stats Data")
        canvas.itemconfig(t2, text="")

        # Game information values.
        self.texts["score"] = canvas.create_text(55, 50, fill="cyan")
        canvas.itemconfig(t2, text="Score")
        self.texts["level"] = canvas.create_text(110, 50, fill="cyan")
        canvas.itemconfig(t2, text="Level")
        self.texts["speed"] = canvas.create_text(165, 50, fill="cyan")
        canvas.itemconfig(t2, text="Speed")
        self.texts["lives"] = canvas.create_text(220, 50, fill="cyan")
        canvas.itemconfig(t2, text="Lives")

        self.texts["shiptp"] = canvas.create_text(w-20, 10, fill="cyan")
        canvas.itemconfig(t2, text="Teleports")
        self.texts["diamond"] = canvas.create_text(w-20, 30, fill="cyan")
        canvas.itemconfig(t2, text="Diamonds")
        self.texts["coin"] = canvas.create_text(w-20, 50, fill="cyan")
        canvas.itemconfig(t2, text="Coins")
        self.texts["level-view"] = canvas.create_text(GAME_CONFIG["MiddleX"], GAME_CONFIG["MiddleY"],
                                                      fill='Orange',
                                                      font=Font("Helvetica", 46).get_tuple())
        canvas.itemconfig(t2, text="Level View")

        self.background = CPanel(canvas, 0, 71, "extend", "expand", fill="#00a7a7", outline="#00a7a7")

        self.canvas = canvas

        LoadCompleteEvent.bind(self.on_loadcomplete)
        EffectApplyEvent.bind(self.on_effect_apply)

        bubbles = SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"].copy()
        SAVE_DATA["Entities"]["qbubbles:bubble"]["objects"] = []
        for bubble in bubbles:
            bub = Registry.get_bubble(bubble["ID"])
            pos = bubble["Position"]
            x = pos[0]
            y = pos[1]
            rad = bubble["Attributes"]["radius"]
            spd = bubble["Attributes"]["speed"]
            hlt = bubble["Attributes"]["health"]
            bub_obj = Bubble(bub, bub.hardness)
            self.create_bubble(x, y, bub_obj)

        self.player = Player()
        if SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"]:
            self.player.create(*SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0]["Position"])
            self.player.reload(SAVE_DATA["Entities"]["qbubbles:player"]["objects"][0])
        else:
            self.player.create(GAME_CONFIG["MiddleX"], GAME_CONFIG["MiddleY"])
        self._gameobjects.append(self.player)

    def on_pause(self, evt: PauseEvent):
        self._pause = evt.pause

    def on_update(self, evt: UpdateEvent):
        if self._pause:
            return

        if len(self._bubbles) < self.maxBubbles:
            # bubbleObject = self.get_random_bubble()
            # w = GAME_CONFIG["WindowWidth"]
            # h = GAME_CONFIG["WindowHeight"]
            #
            # x = w + bubbleObject.radius
            # y = self.randoms["qbubbles:bubble.y"][0].randint(71 + bubbleObject.radius, h - bubbleObject.radius)
            # self.create_bubble(x, y, bubbleObject)
            self.create_random_bubble()
        self.canvas.itemconfig(self.texts["score"], text=f"{self.player.score}")
        self.canvas.itemconfig(self.texts["level"], text=f"{self.player.get_objectdata()['Attributes']['level']}")
        self.canvas.itemconfig(self.texts["lives"], text=f"{round(self.player.health, 1)}")
        self.canvas.itemconfig(self.texts["score"], text=f"{self.player.score}")

        move_left = 0
        for appliedeffect, dict_ in self.effectImages.copy().items():

            self.canvas.itemconfig(dict_["time"],
                                   text=str(TimeLength(appliedeffect.get_remaining_time())))

            self.canvas.move(dict_["icon"], -move_left, 0)
            self.canvas.move(dict_["time"], -move_left, 0)
            self.canvas.move(dict_["text"], -move_left, 0)
            self.canvas.move(dict_["effectBar"], -move_left, 0)

            if appliedeffect.get_remaining_time() < 0:
                appliedeffect.dead = True

            if appliedeffect.dead:
                self.canvas.delete(dict_["icon"])
                self.canvas.delete(dict_["time"])
                self.canvas.delete(dict_["text"])
                self.canvas.delete(dict_["effectBar"])

                del self.effectImages[appliedeffect]
                move_left += 256
                self.effectX -= 256

        # self.texts["score"] = self.player.score
        for bubble in self._bubbles.copy():
            bubble: Bubble
            if not bubble.dead:
                # print((-bubble.radius))
                if bubble.get_coords()[0] < -bubble.baseRadius:
                    bubble.instant_death()
            else:
                self._gameobjects.remove(bubble)
                self._bubbles.remove(bubble)

    def create_random_bubble(self, *, x=None, y=None):
        bubbleObject = self.get_random_bubble()
        w = GAME_CONFIG["WindowWidth"]
        h = GAME_CONFIG["WindowHeight"]

        if x is None:
            x = self.randoms["qbubbles:bubble.x"][0].randint(0 - bubbleObject.radius, w + bubbleObject.radius)
        if y is None:
            y = self.randoms["qbubbles:bubble.y"][0].randint(71 + bubbleObject.radius, h - bubbleObject.radius)
        self.create_bubble(x, y, bubbleObject)

    def on_loadcomplete(self, evt: LoadCompleteEvent):
        UpdateEvent.bind(self.on_update)
        PauseEvent.bind(self.on_pause)
        # CleanUpEvent.bind(self.on_cleanup)
        GameExitEvent.bind(self.on_gameexit)
        LoadCompleteEvent.unbind(self.on_loadcomplete)
        SaveEvent.bind(self.on_save)
        _gameIO.Logging.info("Gamemap", "Load Complete")

        self.player.activate_events()

    def on_gameexit(self, evt: GameExitEvent):
        _gameIO.Logging.info("Gamemap", "Exiting Game - Game Map")
        UpdateEvent.unbind(self.on_update)
        PauseEvent.unbind(self.on_pause)
        # CleanUpEvent.unbind(self.on_cleanup)
        GameExitEvent.unbind(self.on_gameexit)
        SaveEvent.unbind(self.on_save)

        self._bubbles = []
        self.player.deactivate_events()

    def on_save(self, evt: SaveEvent):
        save_path = f"{LAUNCHER_CONFIG['gameDir']}saves/{evt.saveName}"
        return self.save_savedata(save_path)

        # game_data = SAVE_DATA["Game"].copy()
        # game_data["GameType"]["Randoms"] = self.randoms
        # entities_data = SAVE_DATA["Entities"].copy()
        # entity_info_data = SAVE_DATA["EntityInfo"].copy()
        #
        # save_path = f"{LAUNCHER_CONFIG['gameDir']}saves/{evt.saveName}"
        #
        # game_data_file = NZTFile(f"{save_path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()
        #
        # entity_info_file = NZTFile(f"{save_path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()
        #
        # os.makedirs(f"{save_path}/entities/")
        #
        # for entity in entities_data.keys():
        #     path = '/'.join(entity.split(":")[:-1])
        #     os.makedirs(f"{save_path}/entities/{path}")
        #     entity_data_file = NZTFile(f"{save_path}/entities/{entity.replace(':', '/')}.dill", "w")
        #     entity_data_file.data = entities_data[entity]
        #     entity_data_file.save()
        #     entity_data_file.close()

    def load_savedata(self, path):
        SAVE_DATA["EntityInfo"] = Reader(f"{path}/entityinfo.dill").get_decoded()
        SAVE_DATA["Entities"] = {}
        self.maxBubbles = SAVE_DATA["EntityInfo"]["qbubbles:bubble"]["maxAmount"]

        # Get Entity data
        for entity_id in SAVE_DATA["EntityInfo"]["Entities"]:
            entity_path = entity_id.replace(":", "/")
            data = Reader(f"{path}/entities/{entity_path}.dill").get_decoded()
            SAVE_DATA["Entities"][entity_id] = data

    def save_savedata(self, path):
        save_data = SAVE_DATA.copy()

        # entity_data2 = []

        # Transform Object data into List / Dict data.
        for entity in Registry.get_entities():
            save_data["Entities"][entity.get_sname()]["objects"] = []
            _gameIO.Logging.debug("GameTypeSaving", f"EntityData: {save_data['Entities'][entity.get_sname()]}")
        for entity in self.get_gameobjects():
            save_data["Entities"][entity.get_sname()]["objects"].append(entity.get_objectdata())
        for entity in Registry.get_entities():
            _gameIO.Logging.debug("GameTypeSaving", f"EntityData: {save_data['Entities'][entity.get_sname()]}")
        #  = entity_data2

        game_data = save_data["Game"].copy()
        entity_info_data = save_data["EntityInfo"].copy()
        entity_data = save_data["Entities"].copy()

        with open(f"{path}/game.dill", "wb+") as file:
            dill.dump(game_data, file)
            file.close()
        # game_data_file = NZTFile(f"{path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()

        with open(f"{path}/entityinfo.dill", "wb+") as file:
            dill.dump(entity_info_data, file)
            file.close()

        # entity_info_file = NZTFile(f"{path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()

        if not os.path.exists(f"{path}/entities/"):
            os.makedirs(f"{path}/entities/")

        for entity in entity_data.keys():
            entity_path = '/'.join(entity.split(":")[:-1])
            if not os.path.exists(f"{path}/entities/{entity_path}"):
                os.makedirs(f"{path}/entities/{entity_path}", exist_ok=True)

            with open(f"{path}/entities/{entity.replace(':', '/')}.dill", "wb+") as file:
                dill.dump(entity_data[entity], file)
                file.close()

            # entity_data_file = NZTFile(
            #     f"{path}/entities/"
            #     f"{entity.replace(':', '/')}.dill",
            #     "w")
            # entity_data_file.data = entity_data[entity]
            # entity_data_file.save()
            # entity_data_file.close()

    def create_savedata(self, path, seed):
        game_data = {
            "GameInfo": {
                "seed": seed
            },
            "GameType": {
                "id": self.get_uname(),
                "seed": seed,
                "initialized": False,
                "Randoms": []
            }
        }

        def dict_exclude_key(key, d: dict):
            d2 = d.copy()
            del d2[key]
            return d2

        entityinfo_data = {
            "qbubbles:bubble": {
                "speedMultiplier": 5,
                "maxAmount": 100
            },
            "Entities": [
                entity.get_sname() for entity in Registry.get_entities()
            ],
            "EntityData": dict(((s.get_sname(), dict_exclude_key("objects", dict(s.get_entitydata()))) for s in Registry.get_entities()))
        }

        entity_data = dict()
        for entity in Registry.get_entities():
            entity_data[entity.get_sname()] = entity.get_entitydata()

        SAVE_DATA = {"GameData": game_data, "EntityInfo": entityinfo_data, "EntityData": entity_data}

        bubble_data = {"bub-id": [], "bub-special": [], "bub-action": [], "bub-radius": [], "bub-speed": [],
                       "bub-position": [], "bub-index": [], "key-active": False}

        with open(f"{path}/game.dill", "wb+") as file:
            dill.dump(game_data, file)
            file.close()
        # game_data_file = NZTFile(f"{path}/game.dill", "w")
        # game_data_file.data = game_data
        # game_data_file.save()
        # game_data_file.close()

        with open(f"{path}/entityinfo.dill", "wb+") as file:
            dill.dump(entityinfo_data, file)
            file.close()

        # entity_info_file = NZTFile(f"{path}/entityinfo.dill", "w")
        # entity_info_file.data = entity_info_data
        # entity_info_file.save()
        # entity_info_file.close()

        os.makedirs(f"{path}/entities/", exist_ok=True)

        for entity in entity_data.keys():
            entity_path = '/'.join(entity.split(":")[:-1])
            if not os.path.exists(f"{path}/entities/{entity_path}"):
                os.makedirs(f"{path}/entities/{entity_path}", exist_ok=True)

            with open(f"{path}/entities/{entity.replace(':', '/')}.dill", "wb+") as file:
                dill.dump(entity_data[entity], file)
                file.close()

        with open(f"{path}/bubble.dill", "wb+") as file:
            dill.dump(bubble_data, file)

            # game_data_file.data = bubble_data
            # game_data_file.save()
            # game_data_file.close()
