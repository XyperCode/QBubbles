import tkinter
from typing import Optional, Union

import yaml
from PIL import ImageTk
from overload import overload

import qbubbles
import qbubbles.references
import qbubbles.registries
from qbubbles.addon import Addon


class Translation(object):
    def __init__(self, lang):
        self._lang = lang
        with open(f"lang/{self._lang}.yaml", "r") as file:
            self._langData = yaml.safe_load(file.read())
            file.close()

    # noinspection PyUnusedFunction
    def get_text(self, *args):
        key = '.'.join(args)

        if key in self._langData.keys():
            return self._langData[key]
        return key


# noinspection PyShadowingNames,PyUnusedClass
class AddonTranslation(Translation):
    def __init__(self, addon: Addon, lang):
        super().__init__(lang)
        self._addon = addon
        self._langData = yaml.safe_load(self._addon.zipimport.get_data(f"assets/lang/{self._lang}.yaml"))

    # noinspection PyUnusedFunction
    def get_text(self, *args):
        key = '.'.join(args)

        if key in self._langData.keys():
            return self._langData[key]
        return key


# noinspection PyShadowingNames
class Translator(object):
    cache = {}

    # noinspection PySameParameterValue
    def __init__(self, addon: Optional[Addon], lang):
        self.lang = lang
        self._addon = addon

    def __repr__(self):
        if self._addon is not None:
            return rf"<Translator({repr(self.lang)}, {repr(self._addon.get_addonid())})>"
        else:
            return rf"<Translator({repr(self.lang)}, 'qbubbles')>"

    def get_translation(self, *args: str, addon: Addon) -> str:
        key = '.'.join(args)

        if addon is not None:
            if (addon.get_addonid(), self.lang, tuple(args)) in Translator.cache.keys():
                return Translator.cache[addon.get_addonid(), self.lang, tuple(args)]

            lang_data = yaml.safe_load(self._addon.zipimport.get_data(f"assets/lang/{self.lang}.yaml"))
            if key in lang_data.keys():
                Translator.cache[addon.get_addonid(), self.lang, tuple(args)] = lang_data[key]
                return lang_data[key]
            return key
            # return AddonTranslation(addon, self.lang)

        if ("qbubbles", self.lang) in Translator.cache.keys():
            return Translator.cache["qbubbles", self.lang]

        with open(f"lang/{self.lang}.yaml", "r") as file:
            lang_data = yaml.safe_load(file.read())
            file.close()
        if key in lang_data.keys():
            Translator.cache["qbubbles", self.lang, tuple(args)] = lang_data[key]
            return lang_data[key]
        return key


class NamespacedKey(object):
    def __init__(self, namespace, key):
        self.__namespace = namespace
        self.__key = key

        self._check_names()

    # Static methods
    @staticmethod
    def from_string(s: str):
        if s.count(":") == 0:
            return NamespacedKey.from_key(s)

        namespace, key = s.split(":", 1)
        return NamespacedKey(namespace, key)

    # Check
    def _check_names(self):
        import string

        for char in self.__namespace:
            if char not in string.ascii_letters + string.digits + "_":
                raise ValueError(rf"namespace contains invalid character {repr(char)}")
        for char in self.__key:
            if char not in string.ascii_letters + string.digits + "_":
                raise ValueError(rf"key contains invalid character {repr(char)}")

    # Overrides
    def __str__(self):
        return rf"{self.__namespace}:{self.__key}"

    def __repr__(self):
        return rf"{self.__namespace}:{self.__key}"

    def __eq__(self, other: 'NamespacedKey'):
        if isinstance(other, NamespacedKey):
            return (self.__namespace == other.get_namespace()) and (self.__key == other.get_key())
        elif isinstance(other, str):
            return self.__str__() == other
        else:
            return False

    def __ne__(self, other: 'NamespacedKey'):
        return not (self.__eq__(other))

    def __hash__(self):
        return hash((self.__namespace, self.__key))

    # Properties
    def get_namespace(self):
        return self.__namespace

    def get_key(self):
        return self.__key

    @classmethod
    def from_key(cls, name):
        from inspect import FrameInfo
        from pathlib import Path
        frames = qbubbles.core.Trace.get_current().find_in_path(qbubbles.references.References.INSTANCE.addonsFolder)
        if frames:
            frame = frames[-1]
            frame: FrameInfo
            # found_frames = []
            filename = Path(Path(frame.filename).absolute().as_posix().split(
                qbubbles.references.References.INSTANCE.addonsFolder.absolute().as_posix(), 1)[1]).parts[0]
            path = qbubbles.references.References.INSTANCE.addonsFolder.joinpath(filename).absolute()
            addonid = qbubbles.registries.Addons.get_addonid_from_path(path)
        else:
            addonid = "qbubbles"
        return NamespacedKey(addonid, name)

        # frame.filename.split(str(References.INSTANCE.addonsFolder))


class Registrable(object):
    def __init__(self) -> None:
        super().__init__()

        self.__key: Optional[NamespacedKey] = None

    def set_uname(self, name):
        self.__key = NamespacedKey.from_key(name)

    def get_key(self):
        return self.__key


# noinspection PyUnusedClass,PyUnusedFunction
class Multiplier(object):
    def __init__(self, default=1):
        self.__key: Optional[NamespacedKey] = None
        self.__value: int = default

    def set_value(self, value):
        self.__value = value

    def get_value(self):
        return self.__value

    @overload
    def set_uname(self, name: str):
        if self.__key is not None:
            self.__key = NamespacedKey.from_key(name)
        else:
            raise PermissionError("access to set key is denied because it's already set")

    @set_uname.add
    def set_uname(self, key: NamespacedKey):

        if self.__key is not None:
            self.__key = key
        else:
            raise PermissionError("access to set key is denied because it's already set")


if __name__ == '__main__':
    from pathlib import Path

    print(Path(Path("/ddfile").absolute().as_posix().split(Path("/dir/to").absolute().as_posix() + "/", 1)[1]).parts[0])

    import uuid

    print(uuid.uuid3(uuid.NAMESPACE_X500, 'Hallo meneer Opa'))


def generate_uuid(s: str):
    return uuid.uuid3(uuid.NAMESPACE_X500, s)


PhotoImage = Union[ImageTk.PhotoImage, tkinter.PhotoImage]
