from abc import ABC
from pathlib import Path
from typing import Type, Optional
from zipimport import zipimporter

from qbubbles.globals import GAME_VERSION


class AddonInfo(object):
    def __init__(self, id_, name, version, fp, mod_path):
        self.addonID = id_
        self.name = name
        self.version = version
        self.fp = fp
        self.modPath = mod_path


class AddonSkeleton(ABC):
    addonID: str
    name: str
    version: str
    fpath: Path
    modPath: Path
    zipimport: Optional[zipimporter]

    def __str__(self):
        return self.addonID

    def __repr__(self):
        return f"Addon(<{self.addonID}>)"

    def get_addonid(self):
        return self.addonID

    def get_addoninfo(self) -> AddonInfo:
        return AddonInfo(self.addonID, self.name, self.version, self.fpath, self.modPath)

    def get_zipimport(self):
        return self.zipimport


class Addon(AddonSkeleton):
    def __init__(self, clazz: Type):
        clazz.INSTANCE = self

    @classmethod
    def register(cls, *, addonid, name, version, qbversion=GAME_VERSION):
        import string
        if addonid[0] not in string.ascii_letters:
            raise ValueError(f"Invalid character of addon ID {repr(addonid)}: '{addonid[0]}' must be a ASCII letter")
        for character in addonid[1:]:
            if character not in string.ascii_letters+string.digits:
                raise ValueError(f"Invalid character of addon ID {repr(addonid)}: '{character}' must be a ASCII letter")
        # print(addonid, name, version, qbversion)

        def decorator(clazz):
            # Imports
            import os
            from qbubbles import Addons
            from inspect import getfile, isclass

            # Check is class.
            if not isclass(clazz):
                raise TypeError(f"Object '{clazz.__name___}' is not a class")

            # Check game version.
            if qbversion == GAME_VERSION:
                # MODS.append(dict(addonid=addonid, name=name, version=version, func=func))
                clazz.addonID = addonid
                clazz.name = name
                clazz.fpath = getfile(clazz)
                clazz.modPath = os.path.split(clazz.fpath)[0]
                clazz.version = version
                clazz.zipimport = None
                Addons.register(clazz, clazz.modPath)
        return decorator

