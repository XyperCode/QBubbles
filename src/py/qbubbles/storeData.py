import warnings

warnings.warn("Call to deprecated module.", DeprecationWarning)


class StoreItem(object):
    def __init__(self, coins, diamonds, name, icon):
        self.coins = coins
        self.diamonds = diamonds
        self.name = name
        self.icon = icon
